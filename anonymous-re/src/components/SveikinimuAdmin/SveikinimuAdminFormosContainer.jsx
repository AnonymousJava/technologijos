import React, { Component } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';
import baseUrl from '../../AppConfig';

import AdminFormComponent from './SveikinimuAdminFormosComponent';

class AdminFormContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paveiksliukas: '',
            vardas: '',
            pavarde: '',
            tekstas: '',
            audioUrl: '',
            id: '',
            tipas: ''
        }
    }

    componentDidMount() {
        if (this.props.match.params.id > 0) {
            axios
                .get(`${baseUrl}/api/sveikinimai/${this.props.match.params.id}`)
                .then(res => this.setState({
                    id: res.data.id || '',
                    paveiksliukas: res.data.paveiksliukas || '',
                    tekstas: res.data.tekstas || '',
                    audioUrl: res.data.audioIrasoUrl || '',
                    vardas: res.data.vardas || '',
                    pavarde: res.data.pavarde || '',
                    tipas: res.data.tipas || ''
                }))
                .catch(err => console.log(err))
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();

        if (this.state.id) {
            axios.put(`${baseUrl}/api/sveikinimai/${this.props.match.params.id}`, {
                'paveiksliukas': this.state.paveiksliukas || '',
                'tekstas': this.state.tektas || '',
                'audioIrasoUrl': this.state.audioIrasoUrl || '',
                'vardas': this.state.vardas || '',
                'pavarde': this.state.pavarde || '',
                'tipas': this.state.tipas || ''
            }).then(() => this.props.history.push('/admin/sveikinimai'))
        } else {
            axios.post(`${baseUrl}/api/sveikinimai`, {
                'paveiksliukas': this.state.paveiksliukas || '',
                'tekstas': this.state.tektas || '',
                'audioIrasoUrl': this.state.audioIrasoUrl || '',
                'vardas': this.state.vardas || '',
                'pavarde': this.state.pavarde || '',
                'tipas': this.state.tipas || ''
            }).then(() => this.props.history.push('/admin/sveikinimai'))

            this.setState({
                id: '',
                paveiksliukas: '',
                tekstas: '',
                audioUrl: '',
                vardas: '',
                pavarde: '',
                tipas: ''
            })
        }
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    render() {
        return (
            <AdminFormComponent handleSubmit={this.handleSubmit} handleChange={this.handleChange} {...this.state} />
        )
    }
}

export default withRouter(AdminFormContainer);
