import React from 'react'

const ProductAdministrationFormComponent = ({ handleChange, handleSubmit, ...otherProps }) => {
    const { paveiksliukas, tekstas, audioUrl, vardas, pavarde, tipas } = otherProps;
    const tipai = ["GYVAS_PASTATE", "TV", "INTERNETINIS"];
    return (
        <div>
            <form className='container my-5' onSubmit={handleSubmit}>
                <div className='form-group'>
                    <label htmlFor='itemPaveiksliukas'>Paveiksliukas</label>
                    <input onChange={handleChange} type='text' className='form-control' id='itemPaveiksliukas' name='paveiksliukas' value={paveiksliukas} required />
                </div>
                <div className='form-group'>
                    <label htmlFor='itemTekstas'>Tekstas</label>
                    <textarea onChange={handleChange} className='form-control' id='itemTekstas' rows='3' name='tekstas' value={tekstas}></textarea>
                </div>
                <div className='form-group'>
                    <label htmlFor='itemAudioUrl'>AudioUrl</label>
                    <input onChange={handleChange} type='url' className='form-control' id='itemAudioUrl' name='audioUrl' value={audioUrl} />
                </div>
                <div className='form-group'>
                    <label htmlFor='vardas'>Vardas</label>
                    <input onChange={handleChange} type='text' className='form-control' id='vardas' name='vardas' value={vardas} required />
                </div>
                <div className='form-group'>
                    <label htmlFor='pavarde'>Pavarde</label>
                    <input onChange={handleChange} type='text' className='form-control' id='pavarde' name='pavarde' value={pavarde} required />
                </div>
                <div className="form-group">
                    <label htmlFor="itemCategory">Tipas {tipas && <span className="text-info">({tipas})</span>}</label>
                    <select className="form-control" id="itemCategory">
                        <option value={tipai[0]}>{tipai[0]}</option>
                        <option value={tipai[1]}>{tipai[1]}</option>
                        <option value={tipai[2]}>{tipai[2]}</option>
                        <option value={tipai[3]}>{tipai[3]}</option>
                    </select>
                </div>
                <button type='submit' className='btn btn-primary'>Save</button>
            </form>
        </div >
    )
}

export default ProductAdministrationFormComponent;
