import React from 'react';
import { Link } from 'react-router-dom';
import winterImg from '../../images/winter.jpg'
import defaultImg from '../../images/default.png'

import ModalComponent from '../Modal/ModalComponent';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

const SveikinimuAdminLentelesComponent = ({ sveikinimai, deleteItem }) => {

    return (
        sveikinimai.map(({ id, paveiksliukas, vardas, pavarde }, index) => {
            const imgSrc = paveiksliukas === '/winter.jpg' ? winterImg : defaultImg;

            return (
                <tr key={id}>
                    <th scope='row'>{index + 1}</th>
                    <td>
                        <img
                            src={imgSrc}
                            className='card-img-top'
                            style={{ width: 50, height: 50 }}
                            alt="paveiksliukas"
                        />
                    </td>
                    <td>{vardas} {pavarde}</td>
                    <td>nera</td>
                    <td>
                        <Link className='text-decoration-none mr-3'
                            to={`/admin/sveikinimai/${id}`}>Redaguoti <FontAwesomeIcon icon={faPencilAlt} />
                        </Link>
                        <button className='btn btn-danger' data-toggle="modal" data-target={`#staticBackdrop${id}`} value={id}>
                            Istrinti  <FontAwesomeIcon icon={faTrashAlt} />
                        </button>
                    </td>
                    <td>
                        <ModalComponent
                            itemId={id}
                            deleteItem={deleteItem}
                        />
                    </td>
                </tr >
            );
        }))
}

export default SveikinimuAdminLentelesComponent;