import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import baseUrl from '../../AppConfig';

import SveikinimuAdminLentelesComponent from './SveikinimuAdminLentelesComponent';

class SveikinimuAdminLenteleContainer extends Component {
    constructor() {
        super();
        this.state = {
            sveikinimai: []
        }
    }

    componentDidMount = () => {
        axios
            .get(`${baseUrl}/api/sveikinimai`)
            .then((res) => this.setState({ sveikinimai: res.data }))
            .catch((err) => console.log(err));
    }

    deleteItem = (e) => {
        e.preventDefault();
        axios
            .delete(`${baseUrl}/api/sveikinimai/${e.target.value}`)
            .then(() => {
                axios
                    .get(`${baseUrl}/api/sveikinimai`)
                    .then((res) => this.setState({ sveikinimai: res.data }))
            })
            .catch(err => console.log(err))
    }

    render() {
        return (
            <div className='container mt-5'>
                <Link to={`/admin/sveikinimai/new`} className='btn btn-primary mb-5'>
                    Prideti
                    </Link>
                <table className='table'>
                    <thead>
                        <tr>
                            <th scope='col'>#</th>
                            <th scope='col'>Paveiksliukas</th>
                            <th scope='col'>Vardas, Pavarde</th>
                            <th scope='col'>Laikas</th>
                            <th scope='col'>Veiksmai</th>
                        </tr>
                    </thead>
                    {this.state.sveikinimai.length > 0 &&
                        <tbody>
                            <SveikinimuAdminLentelesComponent
                                sveikinimai={this.state.sveikinimai}
                                deleteItem={this.deleteItem}
                            />
                        </tbody>}
                </table>
            </div>
        );
    }
}

export default SveikinimuAdminLenteleContainer;