import React, { Component } from 'react';
import axios from 'axios';
import baseUrl from '../../AppConfig';

import LoadingComponent from '../Loading/LoadingComponent';
import VietosListComponent from './VietosListComponent';

class VietosListContainer extends Component {
    constructor() {
        super();
        this.state = {
            vietos: [],
            search: '',
            newVietos: []
        }
    }

    componentDidMount = () => {
        console.log("Mount");
        axios
            .get(`${baseUrl}/api/vietos`)
            .then(res => this.setState({ vietos: res.data }))
            .catch(err => console.log(err))
    }

    handleSearchChange = (e) => {
        this.setState({ search: e.target.value });
        axios
            .get(`${baseUrl}/api/vietos/names/${e.target.value}`)
            .then(res => this.setState({ newVietos: res.data }))
            .catch(err => console.log(err))
    }

    render() {
        if (this.state.vietos.length > 0 && this.state.search.length === 0) {
            return (
                <VietosListComponent
                    vietos={this.state.vietos}
                    handleSearchChange={this.handleSearchChange}
                    search={this.state.search}
                />
            )
        } else if (this.state.vietos.length > 0 && this.state.search.length > 0) {
            return (
                <VietosListComponent
                    vietos={this.state.newVietos}
                    handleSearchChange={this.handleSearchChange}
                    search={this.state.search}
                />
            )
        } else {
            return (
                <LoadingComponent />
            );
        }
    }
}

export default VietosListContainer;