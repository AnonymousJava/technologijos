import React from 'react';
import CardComponent from '../CardComponent/CardComponent';

const VietosListComponent = ({ vietos, search, handleSearchChange }) => (
    <div className='container'>
        <input className='my-3' type='text' value={search} onChange={handleSearchChange} placeholder={'Search'} />
        <div className='row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4'>
            {vietos.map(({ pavadinimas, ...otherProps }) => {
                return (
                    <CardComponent
                        key={pavadinimas}
                        pavadinimas={pavadinimas}
                        {...otherProps}
                    />
                )
            })}
        </div>
    </div >
)

export default VietosListComponent;