import React from 'react';
// import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import winterImg from '../../images/winter.jpg';
import defaultImg from '../../images/default.png';

import '../../styles/Card.css';

const ProductCardComponent = ({ id, tekstas, paveiksliukas, vardas, pavarde, audioUrl, pavadinimas, adresas, logo }) => {
    let imgSrc = paveiksliukas === '/winterImg.jpg' ? winterImg : defaultImg;
    let logoSrc = logo === '/winterImg.jpg' ? winterImg : defaultImg;
    return (
        <div className='col mb-4'>
            <div className='card h-100'>
                {paveiksliukas && <img src={imgSrc} className='card-img-top' alt='paveiksliukas' />}
                {logo && <img src={logoSrc} className='card-img-top' alt='logo' />}
                <div className='card-body'>
                    {pavadinimas && <h5 className='card-title'>{pavadinimas}</h5>}
                    {tekstas && <p className='card-text'>{tekstas}</p>}
                    {audioUrl && <p className='card-text'>{audioUrl}</p>}
                </div>
                <div className='col'>
                    {vardas && <p>Vardas, pavarde: {vardas} {pavarde}</p>}
                    {adresas && <p>Adresas: {adresas}</p>}
                </div>
            </div>
            {id && <Link to={`/sveikinimai/${id}`} className='btn btn-primary'>Detaliau</Link>}
            {pavadinimas && <Link to={`/vietos/${pavadinimas}`} className='btn btn-primary'>Detaliau</Link>}
        </div>
    )
}

// ProductCardComponent.propTypes = {
//     id: PropTypes.number.isRequired,
//     image: PropTypes.string.isRequired,
//     title: PropTypes.string.isRequired,
//     description: PropTypes.string,
//     price: PropTypes.number.isRequired,
//     quantity: PropTypes.number
// };

export default ProductCardComponent;