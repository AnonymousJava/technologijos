import React from 'react';
import { Link } from 'react-router-dom';

const VietosDetailsComponent = ({ vieta }) => {
    const { pavadinimas, adresas } = vieta;

    return (
        <div>
            <div className='media border w-50'>
                <div className='media-body m-3'>
                    <h5 className='mt-0'>{pavadinimas}</h5>
                    <p>{adresas}</p>
                </div>
            </div>
            <div className='row ml-5 mt-3'>
                <Link to={'/vietos'} className='btn btn-vieta'>Back</Link>
            </div>
        </div>

    )
}

export default VietosDetailsComponent;