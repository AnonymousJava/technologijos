import React, { Component } from 'react';
import axios from 'axios';
import baseUrl from '../../AppConfig';

import VietosDetailsComponent from './VietosDetailsComponent';
import LoadingComponent from '../Loading/LoadingComponent';

class VietosDetailsContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            vieta: null
        }
    }

    componentDidMount = () => {
        axios
            .get(`${baseUrl}/api/vietos/${this.props.match.params.pavadinimas}`)
            .then(res => this.setState({ vieta: res.data }))
            .catch(err => console.log(err))
    }

    render() {
        if (this.state.vieta !== null) {
            return (
                <div className='container'>
                    <VietosDetailsComponent
                        vieta={this.state.vieta}
                    />
                </div>
            )
        } else {
            return (
                <LoadingComponent />
            );
        }
    }
}

export default VietosDetailsContainer;