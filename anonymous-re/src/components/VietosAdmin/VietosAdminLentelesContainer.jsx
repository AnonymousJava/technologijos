import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import baseUrl from '../../AppConfig';

import VietosAdminLentelesComponent from './VietosAdminLentelesComponent';

class VietosAdminLentelesContainer extends Component {
    constructor() {
        super();
        this.state = {
            vietos: []
        }
    }

    componentDidMount = () => {
        axios
            .get(`${baseUrl}/api/vietos`)
            .then((res) => this.setState({ vietos: res.data }))
            .catch((err) => console.log(err));
    }

    deleteItem = (e) => {
        e.preventDefault();
        axios
            .delete(`${baseUrl}/api/vietos/${e.target.value}`)
            .then(() => {
                axios
                    .get(`${baseUrl}/api/vietos`)
                    .then((res) => this.setState({ vietos: res.data }))
            })
            .catch(err => console.log(err))
    }

    render() {
        return (
            <div className='container mt-5'>
                <Link to={`/admin/vietos/new`} className='btn btn-primary mb-5'>
                    Prideti
                    </Link>
                <table className='table'>
                    <thead>
                        <tr>
                            <th scope='col'>#</th>
                            <th scope='col'>Pavadinimas</th>
                            <th scope='col'>Adresas</th>
                            <th scope='col'>Logo</th>
                            <th scope='col'>Veiksmai</th>
                        </tr>
                    </thead>
                    {this.state.vietos.length > 0 &&
                        <tbody>
                            <VietosAdminLentelesComponent
                                vietos={this.state.vietos}
                                deleteItem={this.deleteItem}
                            />
                        </tbody>}
                </table>
            </div>
        );
    }
}

export default VietosAdminLentelesContainer;