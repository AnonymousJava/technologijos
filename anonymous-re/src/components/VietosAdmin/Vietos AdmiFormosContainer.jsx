import React, { Component } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';
import baseUrl from '../../AppConfig';

import VietosAdminFormosComponent from './VietosAdminFormosComponent';

class VietosAdminFormosContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pavadinimas: '',
            adresas: '',
            logo: '',
            newPavadinimas: ''
        }
    }

    componentDidMount() {
        if (this.props.match.params.pavadinimas) {
            axios
                .get(`${baseUrl}/api/vietos/${this.props.match.params.pavadinimas}`)
                .then(res => this.setState({
                    pavadinimas: res.data.pavadinimas,
                    adresas: res.data.adresas || '',
                    logo: res.data.logo || ''
                }))
                .catch(err => console.log(err))
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();

        if (this.state.newPavadinimas === '') {
            axios.put(`${baseUrl}/api/vietos/${this.props.match.params.pavadinimas}`, {
                'adresas': this.state.adresas,
                'logo': this.state.logo
            }).then(() => this.props.history.push('/admin/vietos'))
        } else {

            axios.post(`${baseUrl}/api/vietos`, {
                'pavadinimas': this.state.newPavadinimas,
                'adresas': this.state.adresas,
                'logo': this.state.logo
            }).then(() => this.props.history.push('/admin/vietos'))

            this.setState({
                pavadinimas: '',
                adresas: '',
                logo: '',
                newPavadinimas: ''
            })
        }
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    render() {
        return (
            <VietosAdminFormosComponent handleSubmit={this.handleSubmit} handleChange={this.handleChange} {...this.state} />
        )
    }
}

export default withRouter(VietosAdminFormosContainer);
