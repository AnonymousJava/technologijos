import React from 'react'

const VietosAdminFormosComponent = ({ handleChange, handleSubmit, ...otherProps }) => {
    const { pavadinimas, adresas, logo, newPavadinimas } = otherProps;
    return (
        <div>
            <form className='container my-5' onSubmit={handleSubmit}>
                {pavadinimas === '' &&
                    <div className='form-group'>
                        <label htmlFor='newPavadinimas'>Pavadinimas</label>
                        <input onChange={handleChange} type='text' className='form-control' id='newPavadinimas' name='newPavadinimas' value={newPavadinimas} required />
                    </div>}
                <div className='form-group'>
                    <label htmlFor='adresas'>Adresas</label>
                    <input onChange={handleChange} type='text' className='form-control' id='adresas' name='adresas' value={adresas} />
                </div>
                <div className='form-group'>
                    <label htmlFor='logo'>Logo</label>
                    <input onChange={handleChange} className='form-control' id='logo' name='logo' value={logo} />
                </div>
                <button type='submit' className='btn btn-primary mt-3'>Save</button>
            </form>
        </div >
    )
}

export default VietosAdminFormosComponent;
