import React from 'react';
import { Link } from 'react-router-dom';

import ModalComponent from '../Modal/ModalComponent';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

const VietosAdminLentelesComponent = ({ vietos, deleteItem }) => {

    return (
        vietos.map(({ pavadinimas, adresas, logo }, index) => {
            return (
                <tr key={pavadinimas}>
                    <th scope='row'>{index + 1}</th>

                    <td>
                        <Link className='text-decoration-none mr-3'
                            to={`/admin/vietos/${pavadinimas}`}>{pavadinimas} <FontAwesomeIcon icon={faPencilAlt} />
                        </Link>
                    </td>
                    <td>{adresas}</td>
                    <td>{logo}</td>
                    <td>
                        <button className='btn btn-danger' data-toggle="modal" data-target={`#staticBackdrop${pavadinimas}`} value={pavadinimas}>
                            Delete  <FontAwesomeIcon icon={faTrashAlt} />
                        </button>
                    </td>
                    <td>
                        <ModalComponent
                            itemId={pavadinimas}
                            title={pavadinimas}
                            deleteItem={deleteItem}
                        />
                    </td>
                </tr >
            );
        }))
}

export default VietosAdminLentelesComponent;