import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => (
    <nav className='navbar navbar-light navbar-expand-md' style={{ backgroundColor: '#e3f2fd' }}>
        <div className='container'>
            <ul className='nav navbar-nav'>
                <NavLink className='nav-link' exact to='/'>Sveikinimai</NavLink>
                <NavLink className='nav-link' exact to='/vietos'>Vietos</NavLink>
                <NavLink className='nav-link' to={`/admin/sveikinimai`}>Administruoti Sveikinimus</NavLink>
                <NavLink className='nav-link' to={`/admin/vietos`}>Administruoti Vietas</NavLink>
            </ul>
        </div>
    </nav >
)

export default Navigation;