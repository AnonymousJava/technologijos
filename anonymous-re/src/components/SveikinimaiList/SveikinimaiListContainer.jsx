import React, { Component } from 'react';
import axios from 'axios';
import baseUrl from '../../AppConfig';

import LoadingComponent from '../Loading/LoadingComponent';
import SveikinimaiListComponent from './SveikinimaiListComponent';

class SveikinimaiListContainer extends Component {
    constructor() {
        super();
        this.state = {
            sveikinimai: []
        }
    }

    componentDidMount = () => {
        console.log("Mount");
        axios
            .get(`${baseUrl}/api/sveikinimai`)
            .then(res => this.setState({ sveikinimai: res.data }))
            .catch(err => console.log(err))
    }

    render() {
        if (this.state.sveikinimai.length > 0) {
            return (
                <SveikinimaiListComponent
                    sveikinimai={this.state.sveikinimai}
                />
            )
        } else {
            return (
                <LoadingComponent />
            );
        }
    }
}

export default SveikinimaiListContainer;