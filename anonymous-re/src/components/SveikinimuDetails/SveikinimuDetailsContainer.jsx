import React, { Component } from 'react';
import axios from 'axios';
import baseUrl from '../../AppConfig';

import SveikinimuDetailsComponent from './SveikinimuDetailsComponent';
import LoadingComponent from '../Loading/LoadingComponent';

class SveikinimuDetailsContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sveikinimas: null
        }
    }

    componentDidMount = () => {
        console.log(this.props.match.params.id);
        axios
            .get(`${baseUrl}/api/sveikinimai/${this.props.match.params.id}`)
            .then(res => this.setState({ sveikinimas: res.data }))
            .catch(err => console.log(err))
    }

    render() {
        if (this.state.sveikinimas !== null) {
            return (
                <div className='container'>
                    <SveikinimuDetailsComponent
                        sveikinimas={this.state.sveikinimas}
                    />
                </div>
            )
        } else {
            return (
                <LoadingComponent />
            );
        }
    }
}

export default SveikinimuDetailsContainer;