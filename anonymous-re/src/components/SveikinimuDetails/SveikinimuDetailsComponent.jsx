import React from 'react';
import { Link } from 'react-router-dom';

import winterImg from '../../images/winter.jpg';
import defaultImg from '../../images/default.png';

const SveikinimuDetailsComponent = ({ sveikinimas }) => {
    const { paveiksliukas, tekstas, audioUrl, vardas, pavarde } = sveikinimas;

    let imgSrc = paveiksliukas === '/winter.jpg' ? winterImg : defaultImg;
    return (
        <div>
            <div className='media'>
                <img className='align-self-start mr-3' src={imgSrc} alt='paveiksliukas' style={{
                    height: '25rem'
                }} />
                <div className='media-body mt-3'>
                    <h5 className='mt-0'>{vardas} {pavarde}</h5>
                    <p>{tekstas}</p>
                    <p>Audio: {audioUrl}</p>
                </div>
            </div>
            <div className='row ml-5 mt-3'>
                <Link to={'/'} className='btn btn-secondary'>Back</Link>
            </div>
        </div>

    )
}

export default SveikinimuDetailsComponent;