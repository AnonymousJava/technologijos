import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Navigation from './components/Navigation/NavigationComponent';
import NoMatch from './components/NoMatch/NoMatchComponent';

import SveikinimaiListContainer from './components/SveikinimaiList/SveikinimaiListContainer';
import VietosListContainer from './components/VietosList/VietosListContainer';

import SveikinimuDetailsContainer from './components/SveikinimuDetails/SveikinimuDetailsContainer';
import VietosDetailsContainer from './components/VietosDetails/VietosDetailsContainer';

import SveikinimuAdminLenteleContainer from './components/SveikinimuAdmin/SveikinimuAdminLenteleContainer'
import SveikinimuAdminFormosContainer from './components/SveikinimuAdmin/SveikinimuAdminFormosContainer';

import VietosAdminLentelesContainer from './components/VietosAdmin/VietosAdminLentelesContainer'
// import VietosAdminFormosContainer from './components/VietosAdmin/VietosAdminFormosContainer';


import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const App = (props) => (
  <div>
    <Navigation />
    <Switch>
      <Route exact path='/' component={SveikinimaiListContainer} />
      <Route path='/sveikinimai/:id' component={SveikinimuDetailsContainer} />
      <Route path='/sveikinimai' component={SveikinimaiListContainer} />
      <Route path='/vietos/:pavadinimas' component={VietosDetailsContainer} />
      <Route path='/vietos' component={VietosListContainer} />
      <Route path='/admin/sveikinimai/new' component={SveikinimuAdminFormosContainer} />
      <Route path='/admin/sveikinimai/:id' component={SveikinimuAdminFormosContainer} />
      {/* <Route path='/admin/vietos/new' component={VietosAdminFormosContainer} />
      <Route path='/admin/vietos/:pavadinimas' component={VietosAdminFormosContainer} /> */}
      <Route path='/admin/sveikinimai' component={SveikinimuAdminLenteleContainer} />
      <Route path='/admin/vietos' component={VietosAdminLentelesContainer} />
      <Route path='*' component={NoMatch} />
      <Route component={NoMatch} />
    </Switch>
    {props.children}
  </div>
)

export default App;
