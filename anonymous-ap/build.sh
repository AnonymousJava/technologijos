#!/bin/sh
echo "Switching working directory to artifact1-ui"
cd ../anonymous-re/

echo "Installing front end dependencies"
yarn

echo "Building front end project"
yarn run build
echo -e "\n\n\n"

echo "Switching working directory to artifact1"
cd ../anonymous-ap

echo "Deleting /src/main/resources/public/"
rm -rf ./src/main/resources/public/

echo "Copying anonymous-ui to anonymous-ap/src/main/resources/public"
mkdir ./src/main/resources/public/
cp -r ../anonymous-re/build/* ./src/main/resources/public/

mvn clean install --no-transfer-progress

