package lt2020.sveikinimai.Vieta;

import org.springframework.data.jpa.repository.JpaRepository;

import lt2020.sveikinimai.Vieta.kita.Vieta;

public interface VietaDao extends JpaRepository<Vieta, String> {

}
