package lt2020.sveikinimai.Vieta.kita;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lt2020.sveikinimai.Sveikinimas.kita.Sveikinimas;

@Entity
public class Vieta {
	@Id
	@Column(unique=true)
	private String pavadinimas;
	@Column
	private String adresas;
	@Column
	private String logo;
	@OneToMany(mappedBy="vieta", cascade=CascadeType.ALL)
	List<Sveikinimas> sveikinimai = new ArrayList<>();
	
	
	public Vieta() {
	}

	public Vieta(String pavadinimas, String adresas, String logo) {
		this.pavadinimas = pavadinimas;
		this.adresas = adresas;
		this.logo = logo;
	}

	public String getPavadinimas() {
		return pavadinimas;
	}

	public void setPavadinimas(String pavadinimas) {
		this.pavadinimas = pavadinimas;
	}

	public String getAdresas() {
		return adresas;
	}

	public void setAdresas(String adresas) {
		this.adresas = adresas;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public List<Sveikinimas> getSveikinimai() {
		return sveikinimai;
	}

	public void setSveikinimai(List<Sveikinimas> sveikinimai) {
		this.sveikinimai = sveikinimai;
	}

	public void addSveikinimas(Sveikinimas sveikinimas) {
		this.sveikinimai.add(sveikinimas);
		sveikinimas.setVieta(this);
	}
	
}
