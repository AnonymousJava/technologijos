package lt2020.sveikinimai.Vieta.kita;

public class VietaInfo {

	private String pavadinimas;
	private String adresas;
	private String logo;
	
	public VietaInfo() {
	}

	public VietaInfo(String pavadinimas, String adresas, String logo) {
		this.pavadinimas = pavadinimas;
		this.adresas = adresas;
		this.logo = logo;
	}

	public String getPavadinimas() {
		return pavadinimas;
	}

	public void setPavadinimas(String pavadinimas) {
		this.pavadinimas = pavadinimas;
	}

	public String getAdresas() {
		return adresas;
	}

	public void setAdresas(String adresas) {
		this.adresas = adresas;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

}
