package lt2020.sveikinimai.Vieta;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lt2020.sveikinimai.Vieta.kita.Vieta;
import lt2020.sveikinimai.Vieta.kita.VietaInfo;

@Service
public class VietaService {
	
	@Autowired
	private VietaDao vietaDao;

		@Transactional(readOnly = true)
		public List<VietaInfo> getVietos() {
			return vietaDao.findAll()
					.stream()
					.map(db -> new VietaInfo(db.getPavadinimas(),
											 db.getAdresas(),
											 db.getLogo()))
					.collect(Collectors.toList());
		}

		@Transactional
		public void createVieta(VietaInfo vietaInfo) {
			
			Vieta db = new Vieta (vietaInfo.getPavadinimas(),
								  vietaInfo.getAdresas(),
								  vietaInfo.getLogo());
			
			vietaDao.save(db);
			
		}

		@Transactional(readOnly = true)
		public VietaInfo getVieta(String pavadinimas) {
			
			Vieta db = vietaDao.findById(pavadinimas).orElse(null);
			
			return new VietaInfo (db.getPavadinimas(),
					 			  db.getAdresas(),
					 			  db.getLogo());
		}

		@Transactional
		public void updateVieta(String pavadinimas, VietaInfo vietaInfo) {
			
			Vieta db = vietaDao.findById(pavadinimas).orElse(null);

			db.setAdresas(vietaInfo.getAdresas());
			db.setLogo(vietaInfo.getLogo());

			vietaDao.save(db);
		}

		@Transactional
		public void deleteVieta(String pavadinimas) {
			vietaDao.deleteById(pavadinimas);		
		}

}
