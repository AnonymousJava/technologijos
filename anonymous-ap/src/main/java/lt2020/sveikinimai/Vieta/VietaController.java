package lt2020.sveikinimai.Vieta;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lt2020.sveikinimai.Vieta.kita.VietaInfo;

@RestController
@Api(value = "vieta")
@RequestMapping(value = "/api/vietos")
public class VietaController {
	@Autowired
	private VietaService vietaService;

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Gauti vietas", notes = "Grazina visas vietas")
	public List<VietaInfo> getVietas() {
		return vietaService.getVietos();
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Sukurti vieta", notes = "Sukurti nauja vieta")
	public void createVieta(@RequestBody VietaInfo vietaInfo) {
		vietaService.createVieta(vietaInfo);
	}

	@RequestMapping(path = "/{pavadinimas}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Gauti vieta", notes = "Gauti vieta pagal id")
	public VietaInfo getVieta(@PathVariable String pavadinimas) {
		return vietaService.getVieta(pavadinimas);
	}

	@RequestMapping(path = "/{pavadinimas}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ApiOperation(value = "Atnaujinti vieta", notes = "Atnaujinti vietos informacija")
	public void updateVieta(@PathVariable String pavadinimas, @RequestBody VietaInfo vietaInfo) {
		vietaService.updateVieta(pavadinimas, vietaInfo);
	}

	@RequestMapping(path = "/{pavadinimas}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Istrinti vieta", notes = "Istrinti vieta pagal id")
	public void deleteVieta(@PathVariable String pavadinimas) {
		vietaService.deleteVieta(pavadinimas);
	}
}
