package lt2020.sveikinimai.Sveikinimas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lt2020.sveikinimai.Sveikinimas.kita.SveikinimasInfo;

@RestController
@Api(value = "sveikinimas")
@RequestMapping(value = "/api/sveikinimai")
public class SveikinimasController {
	
	@Autowired
	private SveikinimasService sveikinimasService;

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Gauna sveikinimus", notes = "Grazina visus sveikinimus")
	public List<SveikinimasInfo> getSveikinimai() {
		return sveikinimasService.getSveikinimai();
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Sukuria sveikinima", notes = "Sukuria nauja sveikinima")
	public void createSveikinimas(@RequestBody SveikinimasInfo sveikinimasInfo) {
		sveikinimasService.createSveikinimas(sveikinimasInfo);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Gauna sveikinima", notes = "Grazina sveikinima pagal id")
	public SveikinimasInfo getSveikinimas(@PathVariable final long id) {
		return sveikinimasService.getSveikinimas(id); 
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ApiOperation(value = "Atnaujina sveikinima", notes = "Atnaujina sveikinima")
	public void updateSveikinimas(@PathVariable final long id, @RequestBody SveikinimasInfo sveikinimasInfo) {
		sveikinimasService.updateSveikinimas(id, sveikinimasInfo);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Istrina sveikinima", notes = "Istrina sveikinima pagal id")
	public void deleteSveikinimas(@PathVariable final long id) {
		sveikinimasService.deleteSveikinimas(id);
	}
	
	@RequestMapping(path="/test", method = RequestMethod.GET)
	String test() {
		return "Testing...";
	}
}
