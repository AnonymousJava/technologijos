package lt2020.sveikinimai.Sveikinimas;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lt2020.sveikinimai.Sveikinimas.kita.Sveikinimas;
import lt2020.sveikinimai.Sveikinimas.kita.SveikinimasInfo;

@Service
public class SveikinimasService {
	
	@Autowired
	private SveikinimasDao sveikinimasDao;

	@Transactional(readOnly = true)
	public List<SveikinimasInfo> getSveikinimai() {
		return sveikinimasDao.findAll()
				.stream()
				.map(db -> new SveikinimasInfo(db.getId(),
											   db.getTekstas(),
											   db.getPaveiksliukas(),
											   db.getAudioIrasoUrl(),
											   db.getVardas(),
											   db.getPavarde(),
											   db.getTipas()))
				.collect(Collectors.toList());
	}

	@Transactional
	public void createSveikinimas(SveikinimasInfo sveikinimasInfo) {
		
		Sveikinimas sveikinimas = new Sveikinimas (sveikinimasInfo.getTekstas(),
												   sveikinimasInfo.getPaveiksliukas(),
												   sveikinimasInfo.getAudioIrasoUrl(),
												   sveikinimasInfo.getVardas(),
												   sveikinimasInfo.getPavarde(),
												   sveikinimasInfo.getTipas());
		
		sveikinimasDao.save(sveikinimas);	
	}

	@Transactional(readOnly = true)
	public SveikinimasInfo getSveikinimas(long id) {
		Sveikinimas db = sveikinimasDao.findById(id).orElse(null);
		
		return new SveikinimasInfo(db.getId(),
				   				   db.getPaveiksliukas(),
				   				   db.getTekstas(),
				   				   db.getAudioIrasoUrl(),
				   				   db.getVardas(),
				   				   db.getPavarde(),
				   				   db.getTipas());
	}

	@Transactional
	public void updateSveikinimas(long id, SveikinimasInfo sveikinimasInfo) {
		Sveikinimas db = sveikinimasDao.findById(id).orElse(null);
		
			db.setTekstas(sveikinimasInfo.getTekstas());
			db.setPaveiksliukas(sveikinimasInfo.getPaveiksliukas());
			db.setAudioIrasoUrl(sveikinimasInfo.getAudioIrasoUrl());
			db.setVardas(sveikinimasInfo.getVardas());
			db.setPavarde(sveikinimasInfo.getPavarde());
			db.setTipas(sveikinimasInfo.getTipas());
			
		sveikinimasDao.save(db);
	}

	@Transactional
	public void deleteSveikinimas(long id) {
		sveikinimasDao.deleteById(id);		
	}

}
