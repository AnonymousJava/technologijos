package lt2020.sveikinimai.Sveikinimas;

import org.springframework.data.jpa.repository.JpaRepository;

import lt2020.sveikinimai.Sveikinimas.kita.Sveikinimas;

public interface SveikinimasDao extends JpaRepository<Sveikinimas, Long> {

}
