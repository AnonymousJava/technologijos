package lt2020.sveikinimai.Sveikinimas.kita;

public class SveikinimasInfo {

	private long id;
	private String tekstas;
	private String paveiksliukas;
	private String audioIrasoUrl;
	private String vardas;
	private String pavarde;
	private Tipas tipas;

	public SveikinimasInfo() {
	}

	public SveikinimasInfo(String tekstas, String paveiksliukas, String audioIrasoUrl, String vardas,
			String pavarde, Tipas tipas) {
		this.tekstas = tekstas;
		this.paveiksliukas = paveiksliukas;
		this.audioIrasoUrl = audioIrasoUrl;
		this.vardas = vardas;
		this.pavarde = pavarde;
		this.tipas = tipas;
	}
	
	public SveikinimasInfo(long id, String tekstas, String paveiksliukas, String audioIrasoUrl, String vardas,
			String pavarde, Tipas tipas) {
		this.id = id;
		this.tekstas = tekstas;
		this.paveiksliukas = paveiksliukas;
		this.audioIrasoUrl = audioIrasoUrl;
		this.vardas = vardas;
		this.pavarde = pavarde;
		this.tipas = tipas;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTekstas() {
		return tekstas;
	}

	public void setTekstas(String tekstas) {
		this.tekstas = tekstas;
	}

	public String getPaveiksliukas() {
		return paveiksliukas;
	}

	public void setPaveiksliukas(String paveiksliukas) {
		this.paveiksliukas = paveiksliukas;
	}

	public String getAudioIrasoUrl() {
		return audioIrasoUrl;
	}

	public void setAudioIrasoUrl(String audioIrasoUrl) {
		this.audioIrasoUrl = audioIrasoUrl;
	}

	public String getVardas() {
		return vardas;
	}

	public void setVardas(String vardas) {
		this.vardas = vardas;
	}

	public String getPavarde() {
		return pavarde;
	}

	public void setPavarde(String pavarde) {
		this.pavarde = pavarde;
	}

	public Tipas getTipas() {
		return tipas;
	}

	public void setTipas(Tipas tipas) {
		this.tipas = tipas;
	}
	
}
