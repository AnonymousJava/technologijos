package lt2020.sveikinimai.Sveikinimas.kita;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lt2020.sveikinimai.Vieta.kita.Vieta;

@Entity
public class Sveikinimas {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column
	private String tekstas;
	@Column
	private String paveiksliukas;
	@Column
	private String audioIrasoUrl;
	@Column
	private String Vardas;
	@Column
	private String Pavarde;
	@Column
	private Tipas tipas;
	@ManyToOne(cascade= {CascadeType.MERGE, CascadeType.DETACH})
	@JoinColumn(name="vieta_pavadinimas")
	private Vieta vieta;
	

	public Sveikinimas() {
	}

	public Sveikinimas(String tekstas, String paveiksliukas, String audioIrasoUrl, String Vardas,
			String Pavarde, Tipas tipas) {
		this.tekstas = tekstas;
		this.paveiksliukas = paveiksliukas;
		this.audioIrasoUrl = audioIrasoUrl;
		this.Vardas = Vardas;
		this.Pavarde = Pavarde;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTekstas() {
		return tekstas;
	}

	public void setTekstas(String tekstas) {
		this.tekstas = tekstas;
	}

	public String getPaveiksliukas() {
		return paveiksliukas;
	}

	public void setPaveiksliukas(String paveiksliukas) {
		this.paveiksliukas = paveiksliukas;
	}

	public String getAudioIrasoUrl() {
		return audioIrasoUrl;
	}

	public void setAudioIrasoUrl(String audioIrasoUrl) {
		this.audioIrasoUrl = audioIrasoUrl;
	}

	public String getVardas() {
		return Vardas;
	}

	public void setVardas(String Vardas) {
		this.Vardas = Vardas;
	}

	public String getPavarde() {
		return Pavarde;
	}

	public void setPavarde(String Pavarde) {
		this.Pavarde = Pavarde;
	}

	public Tipas getTipas() {
		return tipas;
	}

	public void setTipas(Tipas tipas) {
		this.tipas = tipas;
	}

	public Vieta getVieta() {
		return vieta;
	}

	public void setVieta(Vieta vieta) {
		this.vieta = vieta;
	}
}
